source 'https://rubygems.org'

ruby '2.0.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use postgresql as the database for Active Record
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Twitter bootstrap
gem 'bootstrap-sass'

# Use Haml markup
gem 'haml'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# http request library
gem 'typhoeus'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# User auth
gem 'devise', '~> 3.0.0.rc'

# TODO: Ripping out turbolinks for now
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
#gem 'turbolinks'

# triggers onLoad
#gem 'jquery-turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Enable client-side validations
gem 'client_side_validations', github: 'bcardarella/client_side_validations', branch: '4-0-beta'

group :development do

  #gem 'rails_best_practices'

  gem 'better_errors'

  gem 'quiet_assets'

end

group :test do

  # Gemfile in Rails app
  gem 'mocha', require: false

end

group :production do

  # Enable STDOUT logging in Rails4 for deployment on Heroku
  gem 'rails_12factor'

  # production server
  gem 'unicorn'
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
