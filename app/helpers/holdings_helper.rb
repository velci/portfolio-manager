module HoldingsHelper

  def purchase_value(holding)
    holding.quantity * holding.price
  end

  def computed_value(holding, quote)
    quote.previous_close == 'N/A' ? 'N/A' : (holding.quantity * quote.previous_close.to_f)
  end

  def change(holding, quote)
    computed = computed_value(holding, quote)
    computed == 'N/A' ? 'N/A' : computed - purchase_value(holding)
  end

  def percent_change(holding, quote)
    purchase = purchase_value(holding)
    change = change(holding, quote)
    if change == 'N/A'
      'N/A'
    else
      purchase == 0 ? 0 : (change / purchase) * 100
    end
  end

  def change_text_class(number)

    number = number.to_f

    if number > 0
      'text-success'
    elsif number < 0
      'text-error'
    else
      ''
    end
  end

  def change_icon_class(number)

    number = number.to_f

    if number > 0
      'icon-sort-up'
    elsif number < 0
      'icon-sort-down'
    else
      ''
    end
  end
end
