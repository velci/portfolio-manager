module ApplicationHelper

  def to_currency(number)
    number == 'N/A' ? 'N/A' : number_to_currency(number.to_f)
  end

  def to_percentage(number)
    number == 'N/A' ? 'N/A' : number_to_percentage(number.to_f)
  end

end
