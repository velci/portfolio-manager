module Holdings
  class ShowPresenter
    def initialize(holding)
      @holding = holding
    end

    def quote()
      Quote.find(@holding.symbol).first
    end

    def history(quote)
      QuoteHistory.find(@holding.symbol) if quote.valid?
    end
  end
end