module Holdings
  class IndexPresenter
    def initialize(holdings)
      @holdings = holdings
    end

    def quotes_map()
      quotes = Quote.find(@holdings.map{|holding| holding.symbol})

      holding_quotes = {}

      quotes.each {|quote| holding_quotes[quote.symbol] = quote }

      holding_quotes
    end
  end
end