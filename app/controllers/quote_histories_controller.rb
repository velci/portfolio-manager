class QuoteHistoriesController < ApplicationController
  before_action :set_quote_history, only: [:show]

  respond_to :json

  # GET /quote_histories/1.json
  def show
    render status: :unprocessable_entity if @quote_history.empty?
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote_history
      @quote_history = QuoteHistory.find(params[:id], params[:begin_date], params[:end_date], params[:interval])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_history_params
      params[:quote_history]
    end
end
