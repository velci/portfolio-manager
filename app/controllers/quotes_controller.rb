class QuotesController < ApplicationController
  before_action :set_quote, only: [:show]

  # GET /quotes
  # GET /quotes.json
  def index
  end

  # GET /quotes/1
  # GET /quotes/1.json
  def show
    unless @quote.error == 'N/A'
      flash[:error] = @quote.error
      redirect_to quotes_path
    end
  end

  def search
    redirect_to quote_path params[:symbol]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote
      @quote = Quote.find(params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_params
      params[:quote]
    end
end
