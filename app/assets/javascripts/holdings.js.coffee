# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

renderLineChart = ->

  lineChartPane = $('#line_chart')
  lineChart = $(lineChartPane).find(".js-chart")
  symbol = $(lineChart).data("symbol")

  if lineChart.length > 0

    $.getJSON("/quote_histories/#{symbol}", ->
      console.log "success"
    ).done( (data)->
      series = ([Date.parse(record.date), parseFloat(record.close)] for record in data)

      # Create the chart
      $(lineChart).highcharts "StockChart",
        rangeSelector:
          selected: 1

        title:
          text: "#{symbol} Stock Price"

        series: [
          name: symbol
          data: series
          tooltip:
            valueDecimals: 2
        ]
    ).fail(->
      $(lineChartPane).find('.js-chart-error').show()
    ).always(->
      $(lineChartPane).find('.progress').hide()
    )

renderCandlestickChart = ->

  candlestickChartPane = $('#candlestick_chart')
  candlestickChart = $(candlestickChartPane).find(".js-chart")
  symbol = $(candlestickChart).data("symbol")

  if candlestickChart.length > 0

    $.getJSON("/quote_histories/#{symbol}", ->
      console.log "success"
    ).done( (data) ->
      series = ([Date.parse(record.date), parseFloat(record.open), parseFloat(record.high), parseFloat(record.low), parseFloat(record.close)] for record in data)

      # Create the chart
      $(candlestickChart).highcharts "StockChart",
        chart :
          type: 'candlestick'
          zoomType: 'x'

        rangeSelector:
          selected: 1

        title:
          text: "#{symbol} Stock Price"

        series: [
          name: symbol
          data: series
          tooltip:
            valueDecimals: 2
        ]
    ).fail(->
      $(candlestickChartPane).find('.js-chart-error').show()
    ).always(->
      $(candlestickChartPane).find('.progress').hide()
    )

$(document).ready ->
  renderLineChart()

  $("ul.js-chart-nav a[href$=line_chart]").click ->
    tabPane = $($(this).attr("href"))
    $(tabPane).find(".js-chart").highcharts()?.destroy()
    $(tabPane).find(".progress").show()
    $(tabPane).find(".js-chart-error").hide()
    renderLineChart()

  $("ul.js-chart-nav a[href$=candlestick_chart]").click ->
    tabPane = $($(this).attr("href"))
    $(tabPane).find(".js-chart").highcharts()?.destroy()
    $(tabPane).find(".progress").show()
    $(tabPane).find(".js-chart-error").hide()
    renderCandlestickChart()



