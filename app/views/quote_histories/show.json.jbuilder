json.extract! @quote_history

json.array!(@quote_history) do |quote_history|
  json.extract! quote_history, :date, :open, :high, :low, :close, :volume, :adj_close
end
