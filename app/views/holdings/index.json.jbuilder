json.array!(@holdings) do |holding|
  json.extract! holding, :user_id, :symbol, :price, :quantity
  json.url holding_url(holding, format: :json)
end
