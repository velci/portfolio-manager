class Holding < ActiveRecord::Base

  belongs_to :user

  validates :user_id, presence: true
  validates :symbol, presence: true, length: {in: 1..4}, format: {with: /[A-Z0-9]+/i,
                                                                  message: 'must be comprised of letters and numbers'}
  validates :price, presence: true, numericality: {greater_than: 0}
  validates :quantity, presence: true, numericality: {greater_than: 0}

  def total_paid
    self.price * self.quantity
  end
end
