require 'csv'

class QuoteHistory < ExternalModel

  attr_accessor :date, :open, :high, :low, :close, :volume, :adj_close

  def initialize(hash = {})
    QuoteHistory.attributes.each { |attribute|
      self.send("#{attribute}=", hash[attribute.to_s.titleize])
    }
  end

  def self.find(symbol, begin_date = Time.now - 1.year, end_date = Time.now, interval = :day)

    # parameter defaults
    begin_date ||= Time.now - 1.year
    end_date ||= Time.now
    interval ||= :day

    response = Yahoo.quote_history_locator(symbol, begin_date, end_date, interval)

    results = []

    if response
      CSV.parse(response, :headers => true) do |row|
        results << QuoteHistory.new(row.to_hash)
      end

      results.sort!{|x, y| x.date <=> y.date}
    end

    results
  end
end