require 'csv'

class Quote < ExternalModel

  attr_accessor :name, :symbol, :stock_exchange,
                :ask, :bid, :book_value_per_share, :previous_close,
                :day_low, :day_high, :year_high, :year_low,
                :error, :more_info, :notes

  def initialize(hash = {})
    Quote.attributes.each { |attribute|
      self.send("#{attribute}=", hash[attribute])
    }
  end

  def valid?
    self.error == 'N/A'
  end

  def self.find(symbols)

    response = Yahoo.quote_locator(symbols)

    results = []

    if response

      CSV.parse(response, :headers => Quote.attributes) do |row|
        results << Quote.new(row.to_hash)
      end
    end

    results
  end
end