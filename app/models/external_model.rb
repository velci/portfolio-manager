class ExternalModel
  def self.attr_accessor(*vars)
    @attributes ||= []
    @attributes.concat vars
    super(*vars)
  end

  def self.attributes
    @attributes
  end

  def attributes
    self.class.attributes
  end

  def ==(comparison_object)
    attributes = self.attributes | comparison_object.attributes

    equal = true
    attributes.each { |attribute|
      equal && self.send(attribute) == comparison_object.send(attribute)
    }
  end
end