class Yahoo

  def self.quote_locator(symbols)
    # info fields
    info_field_mapping = {
        :error => :e1,
        :more_info => :i,
        :name => :n,
        :notes => :n4,
        :symbol => :s,
        :stock_exchange => :x,
    }

    # real-time fields
    real_time_field_mapping = {
        :after_hours_change => :c8,
        :ask => :a5,
        :bid => :b5,
        :change => :c6,
        :change_percent => :k2,
        :day_range => :m2,
        :day_value_change => :w4,
        :holdings_gain => :g6,
        :holdings_gain_percent => :g5,
        :holdings_value => :v7,
        :last_trade_with_time => :k1,
        :market_cap => :j3,
        :order_book => :i5,
        :pe_ratio => :r2,
    }

    # the non-realtime equivalents of the real-time fields
    # no equivalent for 'after_hours_change' or 'order_book'
    non_real_time_field_mapping = {
        :ask => :a,
        :bid => :b,
        :change => :c1,
        :change_percent => :k2,
        :day_range => :m,
        :day_value_change => :w1,
        :holdings_gain => :g4,
        :holdings_gain_percent => :g1,
        :holdings_value => :v1,
        :last_trade_with_time => :l,
        :market_cap => :j1,
        :pe_ratio => :r,
    }

    misc_mapping = {
        :day_low => :g,
        :day_high => :h,
        :day_range => :m,
        :open => :o,
        :trade_date => :d2,
        :previous_close => :p,
        :book_value_per_share => :b4,
        :year_high => :k,
        :year_low => :j,
        :year_range => :w
    }

    real_time = false

    field_mapping = real_time ? real_time_field_mapping : non_real_time_field_mapping

    field_mapping.merge!(info_field_mapping).merge!(misc_mapping)

    fields = Quote.attributes.map{ |a| field_mapping[a] }

    response = Typhoeus.get('http://finance.yahoo.com/d/quotes.csv', followlocation: true,
                            params: {
                                s: [symbols].flatten.join('+'),
                                f: fields.join
                            })

    if response.success?
      # yahoo's error messages contain double quotes, which is incorrect CSV syntax
      response.body.gsub(/\(.*\)/, '').gsub(/<a href.*>.*<\/a>/, '')
    end
  end

  def self.quote_history_locator(symbol, begin_date = Time.now - 1.year, end_date = Time.now, interval = :day)
    interval_map = {
        :d => :day,
        :w => :week,
        :m => :month
    }

    # parameter defaults
    begin_date ||= Time.now - 1.year
    end_date ||= Time.now
    interval ||= :day

    # the yahoo api requires month to be between 0 and 11
    response = Typhoeus.get('http://ichart.yahoo.com/table.csv', params: {
        s: symbol,
        a: begin_date.month - 1,
        b: begin_date.day,
        c: begin_date.year,
        d: end_date.month - 1,
        e: end_date.day,
        f: end_date.year,
        g: interval_map[interval] || interval
    })

    if response.success?
      response.body
    end
  end

  def self.company_locator
    # TODO: company search url
    # "http://d.yimg.com/aq/autoc?query=AAP&region=US&lang=en-US&callback=YAHOO.util.ScriptNodeDataSource.callbacks"
  end
end