require 'test_helper'

class QuotesControllerTest < ActionController::TestCase
  setup do
    @quote = Quote.new({
                           name: 'Google',
                           symbol: 'GOOG',
                           ask: 550,
                           bid: 550,
                           previous_close: 550,
                           error: 'N/A'
                       })
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should redirect show quote" do
    get :show, id: '1234'
    assert_response :redirect
  end

  test "should show quote" do
    get :show, id: @quote.symbol
    assert_response :success
  end

end
