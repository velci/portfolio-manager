require 'test_helper'

class HoldingsHelperTest < ActionView::TestCase

  test 'returns correct purchase value' do
    holding = holdings(:joe_goog)

    assert_equal(holding.quantity * holding.price, purchase_value(holding))

  end

  test 'returns correct computed value' do

    holding = holdings(:joe_goog)

    quote = Quote.new({
                          name: 'Google',
                          symbol: 'GOOG',
                          ask: 550,
                          bid: 550,
                          previous_close: 550,
                          error: 'N/A'
                      })

    assert_equal(holding.quantity * quote.ask, computed_value(holding, quote))

  end

  test 'returns correct change' do

    holding = holdings(:joe_goog)

    quote = Quote.new({
                          name: 'Google',
                          symbol: 'GOOG',
                          ask: 550,
                          bid: 550,
                          previous_close: 550,
                          error: 'N/A'
                      })

    assert_equal(holding.quantity * 50, change(holding, quote))

  end

  test 'returns correct percent change' do

    holding = holdings(:joe_goog)

    quote = Quote.new({
                          name: 'Google',
                          symbol: 'GOOG',
                          ask: 550,
                          bid: 550,
                          previous_close: 550,
                          error: 'N/A'
                      })

    assert_equal(10, percent_change(holding, quote))

  end

end
