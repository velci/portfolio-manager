require 'test_helper'

class QuoteHistoryTest < ActiveSupport::TestCase

  test 'find calls the Yahoo quote history locator' do
    symbol = 'GOOG'
    begin_date = Date.parse('2013-01-01')
    end_date = Date.parse('2013-01-01')
    interval = :day

    quote_history = QuoteHistory.new()
    quote_history.date = '2013-01-01'
    quote_history.open = '1'
    quote_history.high = '2'
    quote_history.low = '3'
    quote_history.close = '4'
    quote_history.volume = '5'
    quote_history.adj_close = '6'

    Yahoo.stubs(:quote_history_locator).with(symbol, begin_date, end_date, interval)
      .returns("Date,Open,High,Low,Close,Volume,Adj Close\n2013-01-01,1,2,3,4,5,6")
    assert_equal quote_history, QuoteHistory.find(symbol, begin_date, end_date, interval).first
  end
end
