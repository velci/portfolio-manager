require 'test_helper'

class HoldingTest < ActiveSupport::TestCase

  test 'invalid without a symbol' do
    holding = holdings(:joe_goog)
    holding.symbol = nil
    assert !holding.valid?, 'Symbol presence validation not present on holding'
  end

  test 'invalid without a price' do
    holding = holdings(:joe_goog)
    holding.price = nil
    assert !holding.valid?, 'Price presence validation not present on holding'
  end

  test 'invalid without a quantity' do
    holding = holdings(:joe_goog)
    holding.quantity = nil
    assert !holding.valid?, 'Quantity presence validation not present on holding'
  end

  test 'invalid with a symbol length greater than 4' do
    holding = holdings(:joe_goog)
    holding.symbol = 'GOOGLE'
    assert !holding.valid?, 'Symbol length > 4 validation not present on holding'
  end

  test 'invalid with a price less than 0' do
    holding = holdings(:joe_goog)
    holding.price = -1
    assert !holding.valid?, 'Price < 0 validation not present on holding'
  end

  test 'invalid with a quantity less than 0' do
    holding = holdings(:joe_goog)
    holding.quantity = -1
    assert !holding.valid?, 'Quantity < 0 validation not present on holding'
  end

  test 'valid with attributes' do
    holding = holdings(:joe_goog)
    assert holding.valid?, 'Holding was not valid'
  end


end
