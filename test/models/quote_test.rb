require 'test_helper'

class QuoteTest < ActiveSupport::TestCase

  test 'find calls the Yahoo quote locator' do
    symbol = 'GOOG'

    quote = Quote.new()
    quote.name = 'Google, Inc.'
    quote.symbol = 'GOOG'
    quote.error = 'N/A'
    quote.ask = '892.81'
    quote.bid = '892.12'
    quote.book_value_per_share = '228.01'
    quote.previous_close = '886.43'
    quote.day_low = '889.76'
    quote.day_high = '895.41'
    quote.year_low = '562.09'
    quote.year_high = '920.60'

    Yahoo.stubs(:quote_locator).with(symbol)
    .returns("\"Google, Inc.\",GOOG,N/A,NasdaqNM,892.81,892.12,228.01,886.43,889.76,895.41,562.09,920.60")
    assert_equal quote, Quote.find(symbol).first
  end
end
