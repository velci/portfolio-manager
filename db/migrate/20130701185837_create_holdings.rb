class CreateHoldings < ActiveRecord::Migration
  def change
    create_table :holdings do |t|
      t.integer :user_id
      t.string :symbol
      t.float :price
      t.float :quantity

      t.timestamps
    end
  end
end
