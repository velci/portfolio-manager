# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PortfolioManager::Application.config.secret_key_base = '374b6f3f5a9248789336373ff00062b65cfd1f854a60a307347fa0430a5ef24dfa59a0d2b48772e69c82119a7b9f7f873314364d6a203a8b64428912ea5ffbac'
